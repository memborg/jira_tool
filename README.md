# JIRA Tool

Welcome to the JIRA tool.

#### Features

-   Sprint Throughput forecast
-   PBI leadtime

This tool is a supplement to all the nice reports in JIRA. I have created this
tool because either JIRA is lacking a report type or a certain report type is
missing a few features.

## Usage

### Authorization

To get access to JIRA  we need a Personal Access Token (PAT) from your
JIRA account.

### General usage

You can just run the application and it will guide you through the
authorization. This is only required the first time you run the application.

And when that is completed, you'll be able to generate chart for every team.

Everything else below is focused on explaining the kind of chart and how to
generate a chart from the command line, ie. if you wanted to automate some of
the chart generation.

In general when using the command line interface for passing parameters the 1)
parameter is always the graph type and 2) parameter is always the board you
want data from.

#### Period definition

To make it easier to define period there are a shorthand for it.

##### Absolute Period

You can deal with an absolute period of the for some of the graph types.

For instance if you want data for the first quarter for 2023 your would write
`23q1`.

Valid options: 

* `q` for a quater and 1-4 in value
* `m` for a month and 1-12 in value
* `h` for a half year and 1-2 in value

For a full year you must write a four digit year like `2023`.

##### Relative Period

I you want some thing relative for the current date you have the option to
write something like `2w`. It give you the previous two weeks from Monday in
the current week.

Valid options:

* `w` for week and a positive number

### Generate Sprint Throughput Forecast

This chart is used to forecast how much work a team can do in a fixed time
frame based on historic data.

To generate a SVG Monte Carlo estimation for the Team CAMM execute the
following command:

    jira_tool tp 341 25 10

What is happening here? The command will generate an Monte Carlo forecast based
on how the team have previous performed. The value `25` is the backlog size
that we want an forecast for and `10` is the sprint length we want to base our
forecast on. The forecast will be most precise when there is a larger number of
sprints with the same length.

If you want to see the throughput from a story point perspective you can get
the same diagram by replacing `tp` with `tpsp`. And if you are interested in
how you perform over a certain period you can replace `tp` with `ptp` and
instead of the sprint length you can supply the period you are interested in.
For instance `23m1`. The same goes for story points. Replace `tpsp` with
`ptpsp`.

This will drop a SVG in the same directory as the executable

### Generate PBI Cycle Time Plot

You can generate a scatter plot diagram, which shows how old non-started work
items are.

This is particular useful for discovering old work items, which we have
forgotten.

Lets say you want to generate a cycle time scatter plot for Team CAMM  you
would run this command:

    jira_tool.exe ct 341 23q1

### Generate PBI lead time plot

You can generate a scatter plot diagram, which shows how long time it took for
a work item to be completed from the time it were activated. This is do
identify how long time it takes for work to be registered in the system and to
when it is completed. This could be useful for tracking how long it takes
issues to be closed from the day it is activated.

This is particular useful for a PO when we want to forecast when task might be
done.

    jira_tool lt 341 23q1

### Generate PBI Iteration lead time plot

You can generate a scatter plot diagram, which shows how long time it took for
a work item to be completed from the time it were activated in the current
iteration. This is do identify how long time it takes for work to be registered
in the system and to when it is completed. This could be useful for tracking
how long it takes issues to be closed from the day it is activated.

This is particular useful for a PO when we want to forecast when task might be
done.

    jira_tool slt 341

## Limitations

- You cannot change where the SVG chart is saved.
- It only supports SVG chart generation.
- You will get weird results if you try to access boards where you have no
  access or limited access.

## Contribution

### Requirements

1. First install Rust from <https://www.rust-lang.org/> and follow the
   installation guide.

### Build

1. Run `cargo build` and it will generate an exe in `target\debug\jira_tool`

-   If you add the `--release` parameter it will generate a release build in
    `target\release\jira_tool`

The executable is self contained and has no runtime dependencies once it is
compiled and therefor you can place the executable anywhere on your system and
run it.

