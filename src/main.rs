extern crate chrono;
use chrono::prelude::*;
use chrono::Duration;
use console::Term;
use dialoguer::{theme::ColorfulTheme, Input, Select};
use regex::Regex;
#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate savefile_derive;

extern crate reqwest;
extern crate serde_json;

pub mod boards;
pub mod bucket;
pub mod config;
pub mod graphs;
pub mod issues;
pub mod iterations;
pub mod period;
pub mod plot;
pub mod sprints;
pub mod tools;

use crate::tools::*;
use rand::Rng;
use std::env;
use std::fs::File;
use std::io::prelude::*;

#[derive(Debug)]
pub struct Params {
    pub backlog_size: usize,
    pub board_id: usize,
    pub sprint_len: usize,
    pub chart_type: graphs::GType,
    pub board_name: String,
    pub period: Option<period::Period>,
}

const AGE_RANGE: i32 = 20; //Bubble size. Take into account if the bubbles are to close and collapse them into one
const HOURS_DAY: i32 = 24;

fn main() -> anyhow::Result<()> {
    let args: Vec<_> = env::args().collect();

    if args.len() > 1 {
        cli_params(&args)?;
    } else {
        config::run_setup()?;
        dialoguer_params()?;
    }

    Ok(())
}

fn dialoguer_params() -> anyhow::Result<()> {
    let mut backlog_size: usize = 100;
    let mut _board_id = 341;
    let mut sprint_len: usize = 10;
    let mut chart_type = graphs::GType::Throughput;
    let mut _success = false;
    let mut board_name = String::from("");
    let mut period: Option<period::Period> = None;

    let types = graphs::get_graph_types();

    let c_selection = Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Select chart type")
        .items(&types)
        .interact_on_opt(&Term::stderr())?;

    if let Some(i) = c_selection {
        let c = graphs::get_graph_type(types, i);
        chart_type = c.gtype;
    }

    let input: String = Input::with_theme(&ColorfulTheme::default())
        .with_prompt("Enter the board ID")
        .default("341".into())
        .allow_empty(false)
        .interact_text()?;

    _board_id = input.parse::<usize>().unwrap();

    match boards::get_board(_board_id) {
        Ok(board) => {
            board_name = board.name;
            _success = true;
        }
        Err(e) => {
            _success = false;
            println!("Board {} not found - {}", _board_id, e)
        }
    };

    match chart_type {
        graphs::GType::Throughput => {
            let input: String = Input::with_theme(&ColorfulTheme::default())
                .with_prompt("Backlog size to forecast?")
                .default("20".into())
                .allow_empty(false)
                .interact_text()?;

            backlog_size = input.parse::<usize>().unwrap();

            let input: String = Input::with_theme(&ColorfulTheme::default())
                .with_prompt("Sprint length?")
                .default("10".into())
                .allow_empty(false)
                .interact_text()?;

            sprint_len = input.parse::<usize>().unwrap();
            _success = true;
        }
        graphs::GType::PeriodThroughput => {
            let input: String = Input::with_theme(&ColorfulTheme::default())
                .with_prompt("Backlog size to forecast?")
                .default("20".into())
                .allow_empty(false)
                .interact_text()?;

            backlog_size = input.parse::<usize>().unwrap();

            let input: String = Input::with_theme(&ColorfulTheme::default())
                    .with_prompt("Type in period. Example: 21q1 for first quarter of 2021. 21h1 for first half of 2021. 21m1 for first month of 2021.")
                    .default("21q1".into())
                    .allow_empty(false)
                    .interact_text()?;

            if let Ok(p) = period::get_period(&input) {
                period = Some(p);
            }
            _success = true;
        }
        graphs::GType::ThroughputStoryPoints => {
            let input: String = Input::with_theme(&ColorfulTheme::default())
                .with_prompt("Backlog size in story points to forecast?")
                .default("20".into())
                .allow_empty(false)
                .interact_text()?;

            backlog_size = input.parse::<usize>().unwrap();

            let input: String = Input::with_theme(&ColorfulTheme::default())
                .with_prompt("Sprint length?")
                .default("10".into())
                .allow_empty(false)
                .interact_text()?;

            sprint_len = input.parse::<usize>().unwrap();
            _success = true;
        }
        graphs::GType::PeriodThroughputStoryPoints => {
            let input: String = Input::with_theme(&ColorfulTheme::default())
                .with_prompt("Backlog size in story points to forecast?")
                .default("20".into())
                .allow_empty(false)
                .interact_text()?;

            backlog_size = input.parse::<usize>().unwrap();

            let input: String = Input::with_theme(&ColorfulTheme::default())
                    .with_prompt("Type in period. Example: 21q1 for first quarter of 2021. 21h1 for first half of 2021. 21m1 for first month of 2021.")
                    .default("21q1".into())
                    .allow_empty(false)
                    .interact_text()?;

            if let Ok(p) = period::get_period(&input) {
                period = Some(p);
            }
            _success = true;
        }
        graphs::GType::LeadTime => {
            let input: String = Input::with_theme(&ColorfulTheme::default())
                    .with_prompt("Type in period. Example: 21q1 for first quarter of 2021. 21h1 for first half of 2021. 21m1 for first month of 2021.")
                    .default("21q1".into())
                    .allow_empty(false)
                    .interact_text()?;

            if let Ok(p) = period::get_period(&input) {
                period = Some(p);
            }
            _success = true;
        }
        graphs::GType::CycleTime => {
            let input: String = Input::with_theme(&ColorfulTheme::default())
                    .with_prompt("Type in period. Example: 21q1 for first quarter of 2021. 21h1 for first half of 2021. 21m1 for first month of 2021.")
                    .default("21q1".into())
                    .allow_empty(false)
                    .interact_text()?;

            if let Ok(p) = period::get_period(&input) {
                period = Some(p);
            }
            _success = true;
        }
        graphs::GType::SprintCycleTime => {}
        _ => {
            _success = false;
            println!("Unknow chart type: {:#?}", chart_type);
        }
    };

    let p = Params {
        chart_type,
        backlog_size,
        sprint_len,
        board_id: _board_id,
        board_name,
        period,
    };

    if _success {
        choose_strategi(&p)?;
    }

    Ok(())
}

fn cli_params(args: &[String]) -> std::io::Result<()> {
    let chart_type =
        match graphs::get_graph_type_by_key(graphs::get_graph_types(), args[1].as_str()) {
            Some(s) => s.gtype,
            _ => graphs::GType::None,
        };
    let mut board_id = 341;
    let mut backlog_size: usize = 100;
    let mut sprint_len: usize = 10;
    let mut _success = false;
    let mut board_name = String::from("");
    let mut period: Option<period::Period> = None;

    if args.len() > 2 {
        board_id = args[2].parse().unwrap();
    }

    match boards::get_board(board_id) {
        Ok(board) => {
            board_name = board.name;
            _success = true;
        }
        Err(e) => {
            _success = false;
            println!("Board {} not found - {}", board_id, e)
        }
    };

    match chart_type {
        graphs::GType::Throughput => {
            if args.len() > 4 {
                backlog_size = args[3].parse().unwrap();
                sprint_len = args[4].parse().unwrap();
            }
            _success = true;
        }
        graphs::GType::PeriodThroughput => {
            if args.len() > 4 {
                backlog_size = args[3].parse().unwrap();
                if let Ok(p) = period::get_period(&args[4]) {
                    period = Some(p);
                }
            }
        }
        graphs::GType::ThroughputStoryPoints => {
            if args.len() > 4 {
                backlog_size = args[3].parse().unwrap();
                sprint_len = args[4].parse().unwrap();
            }
            _success = true;
        }
        graphs::GType::PeriodThroughputStoryPoints => {
            if args.len() > 4 {
                backlog_size = args[3].parse().unwrap();
                if let Ok(p) = period::get_period(&args[4]) {
                    period = Some(p);
                }
            }
        }
        graphs::GType::LeadTime => {
            if args.len() > 3 {
                if let Ok(p) = period::get_period(&args[3]) {
                    period = Some(p);
                }
            }
        }
        graphs::GType::SprintCycleTime => {}
        graphs::GType::CycleTime => {
            if args.len() > 3 {
                if let Ok(p) = period::get_period(&args[3]) {
                    period = Some(p);
                }
            }
        }
        _ => {
            _success = false;
            println!("Unknow chart type: {:#?}", chart_type);
        }
    }
    let p = Params {
        backlog_size,
        board_id,
        chart_type,
        sprint_len,
        board_name,
        period,
    };

    if _success {
        choose_strategi(&p)?;
    }

    Ok(())
}

fn choose_strategi(p: &Params) -> std::io::Result<()> {
    match p.chart_type {
        graphs::GType::Throughput => throughput(p, "throughput")?,
        graphs::GType::PeriodThroughput => period_throughput(p, "throughput")?,
        graphs::GType::ThroughputStoryPoints => {
            throughput_story_points(p, "story point throughput")?
        }
        graphs::GType::PeriodThroughputStoryPoints => {
            period_throughput_story_points(p, "story point throughput")?
        }
        graphs::GType::CycleTime => cycle_time(p)?,
        graphs::GType::LeadTime => lead_time(p)?,
        graphs::GType::SprintCycleTime => sprint_cycle_time(p)?,
        _ => {
            println!("Unknow chart type: {:#?}", p.chart_type);
        }
    }

    Ok(())
}

fn period_throughput(p: &Params, measurement: &str) -> std::io::Result<()> {
    let mut sprints: Vec<sprints::Sprint> = Vec::new();

    let mut period_obj = period::Period::create(
        NaiveDate::from_ymd_opt(2020, 1, 1),
        NaiveDate::from_ymd_opt(2030, 12, 31),
        "Forever",
        6,
    );

    if p.period.is_some() {
        period_obj = p.period.clone().unwrap();
    }

    let sprint_len = period_obj.days;
    let issues = issues::get_done_issues(p.board_id, period_obj.clone());

    for i in 0..sprint_len {
        let period_obj = period_obj.clone();
        let period_name = period_obj.name.clone();
        let mut sprint = sprints::Sprint::new();
        sprint.name.clone_from(&period_name);
        sprint.days = 1; //period_obj.days as i32;

        let start = period_obj.start.unwrap();
        let start = start + Duration::days(i);
        let end = start + Duration::days(1);

        let _issues = issues
            .clone()
            .into_iter()
            .filter(|x| {
                tools::naive_datetime_to_datetime_utc(&tools::naive_date_to_naivedatime(&start))
                    <= x.fields.resolutiondate.unwrap()
                    && tools::naive_datetime_to_datetime_utc(&naive_date_to_naivedatime(&end))
                        >= x.fields.resolutiondate.unwrap()
            })
            .collect::<Vec<issues::Issue>>();

        for _issue in _issues {
            sprint.story_points += 1.0;
        }

        sprint.start_date = Some(tools::naive_datetime_to_datetime_utc(
            &tools::naive_date_to_naivedatime(&start),
        ));

        if sprint.story_points > 0.0 {
            sprint.days_per_point = sprint.days as f64 / sprint.story_points;
        }

        sprints.push(sprint);
    }

    monte_carlo(
        sprints,
        p.backlog_size as i32,
        sprint_len as i32,
        &p.board_name,
        measurement,
    )
}

fn throughput(p: &Params, measurement: &str) -> std::io::Result<()> {
    let mut sprints: Vec<sprints::Sprint> = Vec::new();
    let sprint_len = p.sprint_len as i32;
    let mut iters =
        iterations::get_past_iterations_in_range(p.board_id, sprint_len - 2, sprint_len + 2);

    iters.sort_by(|a, b| b.start_date.unwrap().cmp(&a.start_date.unwrap())); //Sort sprint byt start date descending

    iters = iters.drain(..6).collect(); //Work only on the 6 recent sprints.

    for iter in iters.iter() {
        if let Ok(iteration) = iterations::get_sprint_issues(p.board_id, iter.id) {
            let mut sprint = sprints::Sprint::new();
            sprint.name.clone_from(&iter.name);
            sprint.days = iter.days;
            sprint.start_date = iter.start_date;

            for issue in iteration.issues {
                //Take only status category Done (id=3) into consideration for throughput
                if issue.fields.status.status_category.id == 3 {
                    sprint.story_points += 1.0;
                }
            }

            if sprint.story_points > 0.0 {
                sprint.days_per_point = iter.days as f64 / sprint.story_points;
            }

            sprints.push(sprint);
        }
    }

    monte_carlo(
        sprints,
        p.backlog_size as i32,
        p.sprint_len as i32,
        &p.board_name,
        measurement,
    )
}

fn period_throughput_story_points(p: &Params, measurement: &str) -> std::io::Result<()> {
    let mut sprints: Vec<sprints::Sprint> = Vec::new();

    let mut period_obj = period::Period::create(
        NaiveDate::from_ymd_opt(2020, 1, 1),
        NaiveDate::from_ymd_opt(2030, 12, 31),
        "Forever",
        6,
    );

    if p.period.is_some() {
        period_obj = p.period.clone().unwrap();
    }

    let sprint_len = period_obj.days;
    let issues = issues::get_done_issues(p.board_id, period_obj.clone());

    for i in 0..sprint_len {
        let period_obj = period_obj.clone();
        let period_name = period_obj.name.clone();
        let mut sprint = sprints::Sprint::new();
        sprint.name.clone_from(&period_name);
        sprint.days = 1; //period_obj.days as i32;

        let start = period_obj.start.unwrap();
        let start = start + Duration::days(i);
        let end = start + Duration::days(1);

        let _issues = issues
            .clone()
            .into_iter()
            .filter(|x| {
                tools::naive_datetime_to_datetime_utc(&tools::naive_date_to_naivedatime(&start))
                    <= x.fields.resolutiondate.unwrap()
                    && tools::naive_datetime_to_datetime_utc(&naive_date_to_naivedatime(&end))
                        >= x.fields.resolutiondate.unwrap()
            })
            .collect::<Vec<issues::Issue>>();

        for _issue in _issues {
            if let Some(sp) = _issue.fields.story_points {
                sprint.story_points += sp;
            }
        }

        sprint.start_date = Some(tools::naive_datetime_to_datetime_utc(
            &tools::naive_date_to_naivedatime(&start),
        ));

        if sprint.story_points > 0.0 {
            sprint.days_per_point = sprint.days as f64 / sprint.story_points;
        }

        sprints.push(sprint);
    }

    monte_carlo(
        sprints,
        p.backlog_size as i32,
        sprint_len as i32,
        &p.board_name,
        measurement,
    )
}

fn throughput_story_points(p: &Params, measurement: &str) -> std::io::Result<()> {
    let mut sprints: Vec<sprints::Sprint> = Vec::new();
    let sprint_len = p.sprint_len as i32;
    let mut iters =
        iterations::get_past_iterations_in_range(p.board_id, sprint_len - 2, sprint_len + 2);

    iters.sort_by(|a, b| b.start_date.unwrap().cmp(&a.start_date.unwrap())); //Sort sprint byt start date descending

    iters = iters.drain(..6).collect(); //Work only on the 6 recent sprints.

    for iter in iters.iter() {
        if let Ok(iteration) = iterations::get_sprint_issues(p.board_id, iter.id) {
            let mut sprint = sprints::Sprint::new();
            sprint.name.clone_from(&iter.name);
            sprint.days = iter.days;
            sprint.start_date = iter.start_date;

            for issue in iteration.issues {
                //Take only status category Done (id=3) into consideration for throughput
                if issue.fields.status.status_category.id == 3 {
                    if let Some(sp) = issue.fields.story_points {
                        sprint.story_points += sp;
                    }
                }
            }

            if sprint.story_points > 0.0 {
                sprint.days_per_point = iter.days as f64 / sprint.story_points;
            }

            sprints.push(sprint);
        }
    }

    monte_carlo(
        sprints,
        p.backlog_size as i32,
        p.sprint_len as i32,
        &p.board_name,
        measurement,
    )
}

fn monte_carlo(
    sprints: Vec<sprints::Sprint>,
    backlog_size: i32,
    sprint_len: i32,
    team: &str,
    measurement: &str,
) -> std::io::Result<()> {
    let avg: f64 = sprints.iter().map(|x| x.story_points).sum::<f64>() / sprints.len() as f64;
    let avg = round_to_two_decimals(avg);

    let mut sorted_sprints = sprints.clone();

    sorted_sprints.sort_by(|x, y| x.story_points.partial_cmp(&y.story_points).unwrap());

    let median = sorted_sprints.len() / 2;
    let median: f64 = sorted_sprints[median].story_points;

    let mut simulations: Vec<f64> = Vec::new();
    let simulation_runs = 1_000; //The higher the more accurate, but also slower.
    let buckets_num = sprint_len;
    let len = sprints.len();

    for _s in 0..simulation_runs {
        let mut days: f64 = 0.0;
        for _ in 0..backlog_size {
            let i = rand::thread_rng().gen_range(0..len);
            days += sprints[i].days_per_point;
        }

        days = round_to_two_decimals(days);
        simulations.push(days);
    }

    let min_sim = simulations
        .iter()
        .min_by(|x, y| x.partial_cmp(y).unwrap())
        .unwrap();
    let max_sim = simulations
        .iter()
        .max_by(|x, y| x.partial_cmp(y).unwrap())
        .unwrap();
    let diff_sim = round_to_two_decimals(max_sim - min_sim);
    let step_size = round_to_two_decimals(diff_sim / buckets_num as f64);

    let buckets = init_buckets(*min_sim, buckets_num, step_size);
    let buckets = add_to_bucket(simulations, buckets);

    let chance = chance_of_success(&buckets);

    let completed_day = buckets.iter().max_by_key(|x| x.runs).unwrap();

    let filename = clean_filename(&format!("{} - {}.svg", measurement, team));
    let parent = get_exe_root();
    let path = parent.join(filename);
    let path = path.into_os_string().into_string().unwrap();
    let mut file = File::create(&path)?;
    let options = graphs::MCOptions {
        team: team.into(),
        buckets: buckets.clone(),
        avg,
        median,
        sprint_len: sprints.len(),
        chance,
        completed_day: completed_day.max,
        backlog_size,
        working_days: sprint_len,
        measurement: measurement.into(),
    };

    file.write_all(
        graphs::monte_carlo::create_mc_svg(options)
            .into_bytes()
            .as_slice(),
    )?;

    open_file(&path);

    Ok(())
}

fn lead_time(p: &Params) -> std::io::Result<()> {
    let today = Utc::now();
    let start = NaiveDate::from_ymd_opt(today.year(), 1, 1);
    let end = NaiveDate::from_ymd_opt(today.year(), 3, 31);
    let mut start_time = Utc.with_ymd_and_hms(today.year(), 1, 1, 0, 0, 0).unwrap();

    let mut period_obj = period::Period::create(start, end, "", 1);

    if p.period.is_some() {
        period_obj = p.period.clone().unwrap();
        start_time = Utc
            .with_ymd_and_hms(
                period_obj.start.unwrap().year(),
                period_obj.start.unwrap().month(),
                period_obj.start.unwrap().day(),
                0,
                0,
                0,
            )
            .unwrap();
    }

    let mut plots: Vec<plot::PBILeadTime> = Vec::new();

    let issues = issues::get_done_issues(p.board_id, period_obj);

    if cfg!(debug_assertions) {
        let filename = "dump-issues.json";
        let parent = get_exe_root();
        let path = parent.join(filename);
        let path = path.into_os_string().into_string().unwrap();
        let mut file = File::create(path)?;
        file.write_all(
            serde_json::to_string(&issues)
                .unwrap()
                .into_bytes()
                .as_slice(),
        )?;
    }

    let mut count = 0;
    for issue in &issues {
        // let doy = issue.fields.created.unwrap().weekday().number_from_monday() as usize;
        let mut doy = 0;
        if let Some(closed) = issue.fields.closed {
            doy = (closed - start_time).num_days() as usize;
        }
        let age = issue.fields.lead_time as i32 / HOURS_DAY;

        //Take into account if the bubbles are to close and collapse them into one
        if let Some(p) = plots.iter().position(|x| {
            x.doy == doy && x.age as i32 - AGE_RANGE <= age && x.age as i32 + AGE_RANGE >= age
        }) {
            if !plots[p].issues.iter().any(|x| x == &issue.key) {
                plots[p].issues.push(issue.key.clone());
                plots[p].count += 1;
                count += 1;
            }
        } else {
            plots.push(plot::PBILeadTime {
                doy,
                age: age as usize,
                count: 1,
                issues: vec![issue.key.clone()],
            });
            count += 1;
        }
    }

    if cfg!(debug_assertions) {
        let filename = "dump-plots.json";
        let parent = get_exe_root();
        let path = parent.join(filename);
        let path = path.into_os_string().into_string().unwrap();
        let mut file = File::create(path)?;
        file.write_all(
            serde_json::to_string(&plots)
                .unwrap()
                .into_bytes()
                .as_slice(),
        )?;
    }

    cycle_time_ready(plots, p, count, "LeadTime", &issues)
}

fn sprint_cycle_time(p: &Params) -> std::io::Result<()> {
    let iterations = iterations::get_current_iteration(p.board_id);
    if let Some(iteration) = iterations.first() {
        let start = iteration.start_date.unwrap();
        let end = iteration.end_date.unwrap();
        let start = NaiveDate::from_ymd_opt(start.year(), start.month(), start.day());
        let end = NaiveDate::from_ymd_opt(end.year(), end.month(), end.day());

        let period = period::Period::create(start, end, &iteration.name, 6);

        let p = Params {
            board_id: p.board_id,
            backlog_size: p.backlog_size,
            sprint_len: p.sprint_len,
            chart_type: p.chart_type.clone(),
            board_name: p.board_name.clone(),
            period: Some(period),
        };

        return cycle_time(&p);
    }

    Ok(())
}

fn cycle_time(p: &Params) -> std::io::Result<()> {
    let today = Utc::now();
    let start = NaiveDate::from_ymd_opt(today.year(), 1, 1);
    let end = NaiveDate::from_ymd_opt(today.year(), 3, 31);
    let mut start_time = Utc.with_ymd_and_hms(today.year(), 1, 1, 0, 0, 0).unwrap();

    let mut period_obj = period::Period::create(start, end, "", 1);

    if p.period.is_some() {
        period_obj = p.period.clone().unwrap();
        start_time = Utc
            .with_ymd_and_hms(
                period_obj.start.unwrap().year(),
                period_obj.start.unwrap().month(),
                period_obj.start.unwrap().day(),
                0,
                0,
                0,
            )
            .unwrap();
    }

    let mut plots: Vec<plot::PBILeadTime> = Vec::new();

    let issues = issues::get_done_issues(p.board_id, period_obj);
    let mut count = 0;

    if cfg!(debug_assertions) {
        let filename = "dump-issues.json";
        let parent = get_exe_root();
        let path = parent.join(filename);
        let path = path.into_os_string().into_string().unwrap();
        let mut file = File::create(path)?;
        file.write_all(
            serde_json::to_string(&issues)
                .unwrap()
                .into_bytes()
                .as_slice(),
        )?;
    }

    for issue in &issues {
        let mut doy = 0;
        if let Some(closed) = issue.fields.closed {
            doy = (closed - start_time).num_days() as usize;
        }

        let age = issue.fields.cycle_time as i32 / HOURS_DAY;

        //Take into account if the bubbles are to close and collapse them into one
        if let Some(p) = plots.iter().position(|x| {
            x.doy == doy && x.age as i32 - AGE_RANGE <= age && x.age as i32 + AGE_RANGE >= age
        }) {
            if !plots[p].issues.iter().any(|x| x == &issue.key) {
                plots[p].count += 1;
                plots[p].issues.push(issue.key.clone());
                count += 1;
            }
        } else {
            plots.push(plot::PBILeadTime {
                doy,
                age: age as usize,
                count: 1,
                issues: vec![issue.key.clone()],
            });
            count += 1;
        }
    }

    cycle_time_ready(plots, p, count, "CycleTime", &issues)
}

fn cycle_time_ready(
    plots: Vec<plot::PBILeadTime>,
    p: &Params,
    count: usize,
    name: &str,
    issues: &[issues::Issue],
) -> std::io::Result<()> {
    let mut plots2 = plots.clone();
    plots2.sort_by_key(|x| x.age);
    let middle_pos = plots2.len() / 2;

    let mut lines: Vec<plot::PBILeadTimeLines> = Vec::new();

    let age_sum = plots.iter().map(|x| x.age).sum::<usize>();
    let plots_len = count;
    let avg = age_sum / plots_len;
    let avg_count = plots.iter().map(|x| x.age <= avg).len();
    lines.push(plot::PBILeadTimeLines {
        count: avg_count,
        total: count,
        age: avg,
        range: "Average".into(),
        num: 100,
    });

    if middle_pos == 0 {
        if let Some(one) = plots2.get(middle_pos) {
            lines.push(plot::PBILeadTimeLines {
                count: 1,
                total: 1,
                age: one.age,
                range: "100%".into(),
                num: 100,
            });
        }
    } else {
        let (left, _) = plots2.split_at(middle_pos);
        let left_count = left.iter().map(|x| x.count).sum::<usize>();

        let prev_count = left_count;
        let mut _prev_age = 0_i32;
        let age = left.iter().max_by_key(|x| x.age).unwrap().age;

        lines.push(plot::PBILeadTimeLines {
            count: prev_count,
            total: left_count,
            age,
            range: "0-50%".into(),
            num: 50,
        });
        _prev_age = age as i32;

        let middle_pos = (plots2.len() as f64 * 0.70) as usize;
        let (left, _) = plots2.split_at(middle_pos);
        let left_count = left.iter().map(|x| x.count).sum::<usize>();

        let prev_count = left_count - prev_count;
        let age = left.iter().max_by_key(|x| x.age).unwrap().age;

        lines.push(plot::PBILeadTimeLines {
            count: prev_count,
            total: left_count,
            age,
            range: "50-70%".into(),
            num: 70,
        });
        _prev_age = age as i32;

        let prev_count = left_count;

        let middle_pos = (plots2.len() as f64 * 0.85) as usize;
        let (left, _) = plots2.split_at(middle_pos);
        let left_count = left.iter().map(|x| x.count).sum::<usize>();

        let prev_count = left_count - prev_count;
        let age = left.iter().max_by_key(|x| x.age).unwrap().age;

        lines.push(plot::PBILeadTimeLines {
            count: prev_count,
            total: left_count,
            age,
            range: "70-85%".into(),
            num: 85,
        });
        _prev_age = age as i32;
        let prev_count = left_count;

        let middle_pos = (plots2.len() as f64 * 0.95) as usize;
        let (left, _) = plots2.split_at(middle_pos);
        let left_count = left.iter().map(|x| x.count).sum::<usize>();

        let prev_count = left_count - prev_count;
        let age = left.iter().max_by_key(|x| x.age).unwrap().age;

        lines.push(plot::PBILeadTimeLines {
            count: prev_count,
            total: left_count,
            age,
            range: "85-95%".into(),
            num: 95,
        });
        lines.sort_by(|a, b| a.age.partial_cmp(&b.age).unwrap());
    }

    if let Some(period) = p.period.clone() {
        let plot = plot::PBILeadTimePlot {
            value: plots,
            lines,
            count,
            total: 0,
            period,
            issues: issues.to_owned(),
        };

        let filename = clean_filename(&format!("{}-{}.html", name, p.board_name));
        let parent = get_exe_root();
        let path = parent.join(filename);
        let path = path.into_os_string().into_string().unwrap();
        let mut file = File::create(&path)?;

        let title = format!("{} - {}", &p.board_name, name);
        file.write_all(
            graphs::pbileadtime::create_pbileadtime_svg(&title, &plot)
                .into_bytes()
                .as_slice(),
        )?;

        open_file(&path);
    }

    Ok(())
}

fn init_buckets(min: f64, num: i32, step: f64) -> Vec<bucket::Bucket> {
    let mut buckets: Vec<bucket::Bucket> = Vec::new();

    let mut _min = min;
    let mut _max = 0.0;
    for _i in 0..num {
        _max = round_to_two_decimals(_min + step);
        buckets.push(bucket::Bucket {
            min: _min,
            max: _max,
            runs: 0,
        });
        _min = _max;
        _max = 0.0;
    }

    buckets
}

fn add_to_bucket(simulations: Vec<f64>, mut buckets: Vec<bucket::Bucket>) -> Vec<bucket::Bucket> {
    for e in simulations.iter() {
        if let Some(i) = buckets.iter().position(|x| x.min <= *e && x.max >= *e) {
            buckets[i].runs += 1
        }
    }

    buckets
}

fn chance_of_success(buckets: &[bucket::Bucket]) -> f64 {
    let num_buckets = buckets.len();

    let count: i32 = buckets
        .iter()
        .filter(|x| x.max <= num_buckets as f64)
        .map(|x| x.runs)
        .sum();

    round_to_two_decimals(count as f64 / num_buckets as f64)
}

fn get_exe_root() -> std::path::PathBuf {
    let exe = std::env::current_exe().unwrap();
    exe.parent().unwrap().to_path_buf()
}

fn open_file(path: &str) {
    open::that(path).unwrap();
}

fn clean_filename(filename: &str) -> String {
    let fname: String = filename.into();

    let re = Regex::new(r"[<>:/\|?*\x00-\x1F]").unwrap();
    let fname = re.replace_all(&fname, "").into_owned();

    let fname: String = fname.replace('"', "");

    let re = Regex::new(r"^(CON|PRN|AUX|NUL|COM1|COM2|COM3|COM4|COM5|COM6|COM7|COM8|COM9|LPT1|LPT2|LPT3|LPT4|LPT5|LPT6|LPT7|LPT8|LPT9)(\..+)?$").unwrap();
    let fname = re.replace_all(&fname, "").into_owned();

    let fname = trim_whitespace(&fname);

    let re = Regex::new(r"\.$").unwrap();
    let fname = re.replace_all(&fname, "").into_owned();

    if fname.is_empty() {
        return String::from("_");
    }

    fname
}

pub fn trim_whitespace(s: &str) -> String {
    let words: Vec<_> = s.split_whitespace().collect();
    words.join(" ")
}
