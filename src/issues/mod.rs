use crate::config::*;
use crate::period::*;
use chrono::prelude::*;
use chrono::Duration;
use reqwest::header::{HeaderMap, HeaderValue, AUTHORIZATION};

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct Items {
    pub field: String,
    pub fieldtype: String,
    pub from: Option<String>,
    pub to: Option<String>,
    #[serde(rename = "fromString")]
    pub from_string: Option<String>,
    #[serde(rename = "toString")]
    pub to_string: Option<String>,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct Histories {
    pub id: String,
    pub created: Option<DateTime<Utc>>,
    pub items: Vec<Items>,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct Changelog {
    pub histories: Vec<Histories>,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct Resolution {
    pub id: String,
    pub name: String,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct Issuetype {
    pub id: String,
    pub name: String,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct StatusCategory {
    pub id: usize,
    pub key: String,
    pub name: String,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct Status {
    pub id: String,
    pub name: String,
    #[serde(rename = "statusCategory")]
    pub status_category: StatusCategory,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct Fields {
    pub status: Status,
    pub issuetype: Issuetype,
    pub summary: String,
    pub flagged: bool,
    pub resolutiondate: Option<DateTime<Utc>>,
    pub created: Option<DateTime<Utc>>,
    pub updated: Option<DateTime<Utc>>,
    pub resolution: Option<Resolution>,
    pub closed: Option<DateTime<Utc>>,
    pub activated: Option<DateTime<Utc>>,
    #[serde(default)]
    pub closed_day: i32,
    #[serde(default)]
    pub added_day: i32,
    #[serde(default)]
    pub lead_time: i64,
    #[serde(default)]
    pub cycle_time: i64,
    #[serde(rename = "customfield_10002")]
    pub story_points: Option<f64>,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct Issue {
    pub id: String,
    pub key: String,
    pub fields: Fields,
    pub changelog: Changelog,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct Issues {
    #[serde(rename = "maxResults")]
    pub max_results: usize,
    #[serde(rename = "startAt")]
    pub start_at: usize,
    pub total: usize,
    pub issues: Vec<Issue>,
}

pub fn get_new_issues(board_id: usize, p: Period) -> Vec<Issue> {
    let jql = format!(
        "status = {} AND created >= '{}' AND created <= '{}'",
        "Open",
        p.start.unwrap().format("%Y-%m-%d"),
        p.end.unwrap().format("%Y-%m-%d"),
    );

    get(vec![], board_id, &jql, 0, 50)
}

pub fn get_done_issues(board_id: usize, p: Period) -> Vec<Issue> {
    let jql = format!(
        "statuscategory = {} AND resolutiondate >= '{}' AND resolutiondate <= '{}'",
        "Done",
        p.start.unwrap().format("%Y-%m-%d"),
        p.end.unwrap().format("%Y-%m-%d"),
    );

    get(vec![], board_id, &jql, 0, 50)
}

fn construct_headers(config: &Config) -> HeaderMap {
    let mut headers = HeaderMap::new();
    let bearer = format!("Basic {}", config.base64());
    if let Ok(v) = HeaderValue::from_str(&bearer) {
        headers.insert(AUTHORIZATION, v);
    }
    headers
}

fn get(
    mut issues: Vec<Issue>,
    board_id: usize,
    jql: &str,
    start_at: usize,
    max_results: usize,
) -> Vec<Issue> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let str_url = format!(
        "https://elsevier.atlassian.net/rest/agile/1.0/board/{}/issue",
        board_id,
    );

    let sa = start_at.to_string();
    let mx = max_results.to_string();
    let query = vec![
        ("expand", "changelog"),
        (
            "fields",
            "resolution,status,issuetype,resolutiondate,summary,created,updated,flagged,customfield_10002",
        ),
        ("startAt", sa.as_str()),
        ("maxResulst", mx.as_str()),
        ("jql", jql),
    ];

    let url = reqwest::Url::parse(&str_url).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers(&config))
        .query(&query)
        .send()
        .expect("Failed to send request");

    match response.json::<Issues>() {
        Ok(iss) => {
            for issue in iss.issues {
                let mut clone = issue.clone();

                if clone.fields.status.status_category.name == "Done" {
                    clone.fields.closed =
                        get_closed(clone.changelog.histories.clone(), &clone.fields.status.name);
                }

                clone.fields.activated = get_activated(clone.changelog.histories.clone());

                if clone.fields.activated.is_none() {
                    clone.fields.activated = clone.fields.closed;
                }

                if let Some(closed) = clone.fields.closed {
                    clone.fields.closed_day = get_work_day(clone.fields.created.unwrap(), closed);
                }

                if let Some(added_date) = clone.fields.activated {
                    clone.fields.added_day =
                        get_work_day(clone.fields.created.unwrap(), added_date);
                }

                if let Some(cd) = clone.fields.closed {
                    clone.fields.lead_time = (cd - clone.fields.created.unwrap()).num_hours();
                } else {
                    clone.fields.lead_time =
                        (Utc::now() - clone.fields.created.unwrap()).num_hours();
                }

                if let Some(cd) = clone.fields.closed {
                    if let Some(ad) = clone.fields.activated {
                        clone.fields.cycle_time = (cd - ad).num_hours();
                    }
                } else if let Some(ad) = clone.fields.activated {
                    clone.fields.cycle_time = (Utc::now() - ad).num_hours();
                }

                if clone.fields.cycle_time < 24 {
                    clone.fields.cycle_time = 24
                }

                if clone.fields.lead_time < 24 {
                    clone.fields.lead_time = 24
                }

                issues.push(clone);
            }

            if iss.total > start_at {
                let sa = start_at + max_results;
                issues.append(&mut get(vec![], board_id, jql, sa, max_results));
            }
        }
        Err(e) => println!("{:#?}", e),
    };

    issues
}

fn get_activated(histories: Vec<Histories>) -> Option<DateTime<Utc>> {
    let mut dates = get_dates(histories, "In Development");
    dates.sort();

    dates.first().copied()
}

fn get_closed(histories: Vec<Histories>, status: &str) -> Option<DateTime<Utc>> {
    let mut dates = get_dates(histories, status);
    dates.sort();
    dates.reverse();

    dates.first().copied()
}

fn get_dates(histories: Vec<Histories>, state: &str) -> Vec<DateTime<Utc>> {
    let mut dates: Vec<DateTime<Utc>> = Vec::new();
    for h in histories {
        if let Some(created) = h.created {
            for i in h.items {
                if i.to_string == Some(state.into()) && i.field == "status" && i.fieldtype == "jira"
                {
                    dates.push(created);
                }
            }
        }
    }

    dates
}

fn get_work_day(start_date: DateTime<Utc>, end_date: DateTime<Utc>) -> i32 {
    let mut work_days = 0;
    let real_start_date = (start_date + Duration::days(1)) - Duration::seconds(1);

    let mut i = 0;
    loop {
        let new_date = real_start_date + Duration::days(i);

        if new_date > real_start_date {
            match new_date.weekday() {
                Weekday::Sat => {}
                Weekday::Sun => {}
                _ => work_days += 1,
            }
        }

        if new_date >= end_date {
            break;
        }
        i += 1;
    }

    work_days
}
