use crate::config::*;
use reqwest::header::{HeaderMap, HeaderValue, AUTHORIZATION};

#[derive(Clone, Debug, Serialize, Deserialize, Eq, Ord, PartialEq, PartialOrd)]
pub struct Board {
    pub id: usize,
    pub name: String,
    #[serde(rename = "type")]
    pub board_type: String,
}

#[derive(Clone, Debug, Serialize, Deserialize, Eq, Ord, PartialEq, PartialOrd)]
pub struct Boards {
    #[serde(rename = "maxResults")]
    pub max_results: usize,
    #[serde(rename = "startAt")]
    pub start_at: usize,
    pub total: usize,
    #[serde(rename = "isLast")]
    pub is_last: bool,
    pub values: Vec<Board>,
}

fn construct_headers(config: &Config) -> HeaderMap {
    let mut headers = HeaderMap::new();
    let bearer = format!("Basic {}", config.base64());
    if let Ok(v) = HeaderValue::from_str(&bearer) {
        headers.insert(AUTHORIZATION, v);
    }
    headers
}

pub fn get_board(board_id: usize) -> Result<Board, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let str_url = format!(
        "https://elsevier.atlassian.net/rest/agile/1.0/board/{}",
        board_id,
    );
    let url = reqwest::Url::parse(&str_url).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers(&config))
        .send()
        .expect("Failed to send request");

    response.json::<Board>()
}

pub fn get_boards() -> Result<Boards, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let str_url = "https://elsevier.atlassian.net/rest/agile/1.0/board";
    let url = reqwest::Url::parse(str_url).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers(&config))
        .send()
        .expect("Failed to send request");

    response.json::<Boards>()
}
