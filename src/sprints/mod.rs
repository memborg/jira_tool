use chrono::prelude::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Progress {
    pub closed: f64,
    pub remaining: f64,
    pub total: f64,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Sprint {
    pub name: String,
    pub days: i32,
    pub days_left: i32,
    pub days_progress: i32,
    pub start_story_points: f64,
    pub story_points: f64,
    pub days_per_point: f64,
    pub burned_points: f64,
    pub progress: Vec<Progress>,
    pub closed_points: Vec<f64>,
    pub start_date: Option<DateTime<Utc>>,
    pub end_date: Option<DateTime<Utc>>,
}

impl Default for Sprint {
    fn default() -> Sprint {
        Sprint {
            name: String::new(),
            days: 0,
            days_progress: 0,
            days_left: 0,
            start_story_points: 0.0,
            story_points: 0.0,
            days_per_point: 0.0,
            burned_points: 0.0,
            closed_points: Vec::new(),
            progress: Vec::new(),
            start_date: None,
            end_date: None,
        }
    }
}

impl Sprint {
    pub fn new() -> Sprint {
        Sprint::default()
    }
}

impl Default for Progress {
    fn default() -> Progress {
        Progress {
            closed: 0.0,
            remaining: 0.0,
            total: 0.0,
        }
    }
}

impl Progress {
    pub fn new() -> Progress {
        Progress::default()
    }
}
