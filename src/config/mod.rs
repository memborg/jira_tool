extern crate savefile;
use base64::{engine::general_purpose, Engine as _};
use dialoguer::{theme::ColorfulTheme, Input, Password};

#[derive(Savefile)]
pub struct Config {
    pub email: String,
    pub pat: String,
}

pub fn get_config() -> Config {
    match load_file() {
        Ok(c) => Config {
            pat: c.pat,
            email: c.email,
        },
        Err(e) => panic!("Could not load config data! Error: {}", e),
    }
}

impl Config {
    pub fn base64(&self) -> String {
        let b64 = format!("{}:{}", self.email, self.pat);
        general_purpose::STANDARD_NO_PAD.encode(b64)
    }
}

fn load_file() -> Result<Config, savefile::SavefileError> {
    let exe = std::env::current_exe().unwrap();
    let parent = exe.parent().unwrap().to_path_buf();
    let path = parent.join("config.bin");
    let path = path.into_os_string().into_string().unwrap();
    savefile::load_file(path, 0)
}

pub fn run_setup() -> anyhow::Result<()> {
    if load_file().is_err() {
        let email: String = Input::with_theme(&ColorfulTheme::default())
            .with_prompt("JIRA email")
            .allow_empty(false)
            .interact_text()?;
        let pat: String = Password::with_theme(&ColorfulTheme::default())
            .with_prompt("JIRA Personal Access Token")
            .allow_empty_password(false)
            .interact()?;

        let config = Config { pat, email };
        let exe = std::env::current_exe().unwrap();
        let parent = exe.parent().unwrap().to_path_buf();
        let path = parent.join("config.bin");
        let path = path.into_os_string().into_string().unwrap();

        savefile::save_file(path, 0, &config)?
    }

    Ok(())
}
