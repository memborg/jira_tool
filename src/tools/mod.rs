use chrono::prelude::*;
pub fn round_to_two_decimals(number: f64) -> f64 {
    (number * 100.0).round() / 100.0
}

pub fn naive_date_to_naivedatime(naive: &chrono::NaiveDate) -> chrono::NaiveDateTime {
    naive.and_hms_opt(0, 0, 0).unwrap()
}

pub fn naive_datetime_to_datetime_utc(naive: &chrono::NaiveDateTime) -> chrono::DateTime<Utc> {
    let tz_offset = FixedOffset::east_opt(3600).unwrap();
    let td_with_tz: DateTime<FixedOffset> = tz_offset.from_local_datetime(naive).unwrap();

    Utc.from_utc_datetime(&td_with_tz.naive_utc())
}
