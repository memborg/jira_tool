use chrono::prelude::*;
use chrono::Duration;

#[derive(Debug, Clone, Serialize)]
pub struct Period {
    pub start: Option<NaiveDate>,
    pub end: Option<NaiveDate>,
    pub name: String,
    pub days: i64,
    pub modulo: i64,
}

impl Default for Period {
    fn default() -> Period {
        Period {
            start: None,
            end: None,
            name: String::new(),
            days: 0,
            modulo: 6,
        }
    }
}

impl Period {
    pub fn new() -> Period {
        Period::default()
    }

    pub fn create(
        start: Option<NaiveDate>,
        end: Option<NaiveDate>,
        str_period: &str,
        modulo: i64,
    ) -> Period {
        let year = Local::now().year();
        let yy = year - 2000; //Onlyh support this century
        let mut days = 0;

        if let Some(e) = end {
            if let Some(s) = start {
                days = (e - s).num_days();
            }
        }

        Period {
            start,
            end,
            name: format!("{}{}", yy, str_period),
            modulo,
            days,
        }
    }
}

pub fn get_current_quarter() -> Result<Period, &'static str> {
    let year = Local::now().year();
    let month = Local::now().month();

    match month {
        1..=3 => Ok(Period::create(
            NaiveDate::from_ymd_opt(year, 1, 1),
            NaiveDate::from_ymd_opt(year, 3, 31),
            "Q1",
            6,
        )),
        4..=6 => Ok(Period::create(
            NaiveDate::from_ymd_opt(year, 4, 1),
            NaiveDate::from_ymd_opt(year, 6, 30),
            "Q2",
            6,
        )),
        7..=9 => Ok(Period::create(
            NaiveDate::from_ymd_opt(year, 7, 1),
            NaiveDate::from_ymd_opt(year, 9, 30),
            "Q3",
            6,
        )),
        10..=12 => Ok(Period::create(
            NaiveDate::from_ymd_opt(year, 10, 1),
            NaiveDate::from_ymd_opt(year, 12, 31),
            "Q4",
            6,
        )),
        _ => Err("Failed to find current quarter"),
    }
}

pub fn get_previous_quarter() -> Result<Period, &'static str> {
    match get_current_quarter() {
        Ok(p) => {
            let month = p.start.unwrap().month();
            let year = p.start.unwrap().year();

            match month {
                1..=3 => Ok(Period::create(
                    NaiveDate::from_ymd_opt(year - 1, 10, 1),
                    NaiveDate::from_ymd_opt(year - 1, 12, 31),
                    "Q4",
                    6,
                )),
                4..=6 => Ok(Period::create(
                    NaiveDate::from_ymd_opt(year, 1, 1),
                    NaiveDate::from_ymd_opt(year, 3, 31),
                    "Q1",
                    6,
                )),
                7..=9 => Ok(Period::create(
                    NaiveDate::from_ymd_opt(year, 4, 1),
                    NaiveDate::from_ymd_opt(year, 6, 30),
                    "Q2",
                    6,
                )),
                10..=12 => Ok(Period::create(
                    NaiveDate::from_ymd_opt(year, 7, 1),
                    NaiveDate::from_ymd_opt(year, 9, 30),
                    "Q3",
                    6,
                )),
                _ => Err("Failed to find current quarter"),
            }
        }
        _ => Err("Failed to find previous quarter"),
    }
}

pub fn get_current_week() -> Result<Period, &'static str> {
    let now = Local::now();
    let date = NaiveDate::from_ymd_opt(now.year(), now.month(), now.day()).unwrap();
    let week = date.week(Weekday::Mon);

    Ok(Period::create(
        Some(week.first_day()),
        Some(week.last_day()),
        &format!("{}W", date.iso_week().week()),
        1,
    ))
}

pub fn get_previous_n_weeks(n: usize) -> Result<Period, &'static str> {
    let week_days = 7;
    if let Ok(c) = get_current_week() {
        let days = (n * week_days) as i64;
        let start = c.start.unwrap() - Duration::days(days);
        let end = c.start.unwrap() - Duration::days(1);

        return Ok(Period::create(
            Some(start),
            Some(end),
            &format!("{}W", start.iso_week().week()),
            1,
        ));
    }

    Err("Failed to get previous weeks")
}

pub fn get_period(period: &str) -> Result<Period, &'static str> {
    let period = period.to_lowercase();
    let period = period.as_str();

    if period.len() > 3 {
        return get_fixed_period(period);
    } else if period.len() < 4 {
        return get_relative_period(period);
    }

    Err("Invalid Period")
}

fn get_relative_period(period: &str) -> Result<Period, &'static str> {
    if let Some(p) = period.char_indices().nth_back(0) {
        let pos = p.0;
        if let Some(lc) = &period[pos..].chars().next() {
            if lc.is_alphabetic() {
                if let Some(ch) = period.get(0..pos) {
                    if let Ok(num) = ch.parse::<usize>() {
                        match lc {
                            'w' => return get_previous_n_weeks(num),
                            _ => return Err("Unsupported format"),
                        }
                    }
                    return Err("Not starting with a number");
                }
            }

            return Err("Incorrect format should be NNw or similar");
        }
    }

    Err("Failed to parse relative period")
}

fn get_fixed_period(period: &str) -> Result<Period, &'static str> {
    let str_period = String::from("");
    let mut y: i32 = 2000;
    let start_m = 1;
    let end_m = 3;
    let start_d = 1;
    let end_d = 31;
    let modulo = 6;
    if let Some(ch) = period.get(0..2) {
        match ch.parse::<i32>() {
            Ok(num) => {
                y += num;

                let mut last = 0;

                if let Some(ch) = period.get(3..5) {
                    if let Ok(num) = ch.parse::<i32>() {
                        last = num;
                    }
                }

                if last == 0 {
                    if let Some(ch) = period.get(3..4) {
                        if let Ok(num) = ch.parse::<i32>() {
                            last = num;
                        }
                    }
                }

                match period.get(2..3).unwrap() {
                    "h" => return get_half(y, last),
                    "q" => return get_quarter(y, last),
                    "m" => return get_month(y, last),
                    _ => return Err("invalid char - period char"),
                }
            }
            Err(_) => return Err("invalid char - year char"),
        }
    }

    Ok(Period::create(
        NaiveDate::from_ymd_opt(y, start_m as u32, start_d),
        NaiveDate::from_ymd_opt(y, end_m, end_d),
        &str_period,
        modulo,
    ))
}

fn get_half(y: i32, last: i32) -> Result<Period, &'static str> {
    let mut str_period = String::from("");
    let mut start_m = 1;
    let mut end_m = 3;
    let start_d = 1;
    let end_d = 31;
    let half = last;
    if half == 1 {
        start_m = 1;
        end_m = 6;
        str_period = "H1".into();
    }

    if half == 2 {
        start_m = 7;
        end_m = 12;
        str_period = "H2".into();
    }
    let modulo = 12;

    if half > 2 {
        return Err("There can only be two halfs in a year");
    }

    Ok(Period::create(
        NaiveDate::from_ymd_opt(y, start_m as u32, start_d),
        NaiveDate::from_ymd_opt(y, end_m, end_d),
        &str_period,
        modulo,
    ))
}

fn get_quarter(y: i32, last: i32) -> Result<Period, &'static str> {
    let mut str_period = String::from("");
    let mut start_m = 1;
    let mut end_m = 3;
    let start_d = 1;
    let mut end_d = 31;

    if last == 1 {
        start_m = 1;
        end_m = 3;
        str_period = "Q1".into();
    }

    if last == 2 {
        start_m = 4;
        end_m = 6;
        end_d = (NaiveDate::from_ymd_opt(y, end_m + 1, 1).unwrap() - Duration::days(1)).day();
        str_period = "Q2".into();
    }

    if last == 3 {
        start_m = 7;
        end_m = 9;
        end_d = (NaiveDate::from_ymd_opt(y, end_m + 1, 1).unwrap() - Duration::days(1)).day();
        str_period = "Q3".into();
    }

    if last == 4 {
        start_m = 10;
        end_m = 12;
        str_period = "Q4".into();
    }

    let modulo = 6;

    if last < 1 {
        return Err("quarter can not be lower than 1");
    }

    if last > 4 {
        return Err("there can only be 4 quarters in a year");
    }

    Ok(Period::create(
        NaiveDate::from_ymd_opt(y, start_m as u32, start_d),
        NaiveDate::from_ymd_opt(y, end_m, end_d),
        &str_period,
        modulo,
    ))
}

fn get_month(y: i32, last: i32) -> Result<Period, &'static str> {
    if last < 1 {
        return Err("Month number can not be lower than 1");
    }

    if last > 12 {
        return Err("The are only 12 months in a year");
    }

    let mut str_period = String::from("");
    let start_m = last;
    let start_d = 1;
    let m = last;
    let end_m = start_m as u32;
    let mut end_d = 31;
    if last < 12 {
        end_d = (NaiveDate::from_ymd_opt(y, end_m + 1, 1).unwrap() - Duration::days(1)).day();
        str_period = format!("M{}", m);
    }

    if last == 12 {
        end_d = (NaiveDate::from_ymd_opt(y + 1, 1, 1).unwrap() - Duration::days(1)).day();
        str_period = "M12".into();
    }

    let modulo = 3;

    Ok(Period::create(
        NaiveDate::from_ymd_opt(y, start_m as u32, start_d),
        NaiveDate::from_ymd_opt(y, end_m, end_d),
        &str_period,
        modulo,
    ))
}
