use crate::config::*;
use crate::issues::*;
use crate::period::*;
use chrono::prelude::*;
use chrono::Duration;
use reqwest::header::{HeaderMap, HeaderValue, AUTHORIZATION};
use std::fmt;

#[derive(Clone, Debug, Deserialize, Eq, Ord, PartialEq, PartialOrd, Default)]
pub enum State {
    #[default]
    Unknown,
    #[serde(rename = "closed")]
    Closed,
    #[serde(rename = "active")]
    Active,
    #[serde(rename = "future")]
    Future,
}

#[derive(Clone, Default, Debug, Deserialize, Eq, Ord, PartialEq, PartialOrd)]
pub struct Iteration {
    pub id: usize,
    pub state: State,
    pub name: String,
    #[serde(rename = "startDate")]
    pub start_date: Option<DateTime<Utc>>,
    #[serde(rename = "endDate")]
    pub end_date: Option<DateTime<Utc>>,
    #[serde(rename = "completeDate")]
    pub complete_date: Option<DateTime<Utc>>,
    #[serde(rename = "activatedDate")]
    pub activated_date: Option<DateTime<Utc>>,
    // #[serde(rename = "originBoardId")]
    // pub origin_board_id: usize,
    pub goal: Option<String>,
    #[serde(default)]
    pub days: i32,
}

#[derive(Default, Debug, Deserialize, Eq, Ord, PartialEq, PartialOrd)]
pub struct Iterations {
    #[serde(rename = "maxResults")]
    pub max_results: usize,
    #[serde(rename = "startAt")]
    pub start_at: usize,
    #[serde(rename = "isLast")]
    pub is_last: bool,
    pub values: Vec<Iteration>,
}

impl fmt::Display for Iteration {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            State::Active => write!(f, "active"),
            State::Closed => write!(f, "closed"),
            State::Future => write!(f, "future"),
            State::Unknown => write!(f, ""),
        }
    }
}

impl Iterations {
    pub fn new() -> Iterations {
        Iterations::default()
    }
}

fn construct_headers(config: &Config) -> HeaderMap {
    let mut headers = HeaderMap::new();
    let bearer = format!("Basic {}", config.base64());
    if let Ok(v) = HeaderValue::from_str(&bearer) {
        headers.insert(AUTHORIZATION, v);
    }
    headers
}

pub fn get_past_iterations_in_range(board_id: usize, sl_start: i32, sl_end: i32) -> Vec<Iteration> {
    let mut iterations = get_iterations(vec![], board_id, State::Closed, 0, 50);

    iterations = iterations
        .into_iter()
        .filter(|x| x.days >= sl_start && x.days <= sl_end)
        .collect::<Vec<Iteration>>();

    iterations
}

pub fn get_iteration_by_period(board_id: usize, p: Period) -> Vec<Iteration> {
    let mut iterations = get_iterations(vec![], board_id, State::Closed, 0, 50);

    iterations = iterations
        .into_iter()
        .filter(|x| {
            x.start_date.unwrap()
                >= naive_datetime_to_datetime_utc(&naive_date_to_naivedatime(&p.start.unwrap()))
                && x.end_date.unwrap()
                    <= naive_datetime_to_datetime_utc(&naive_date_to_naivedatime(&p.end.unwrap()))
        })
        .collect::<Vec<Iteration>>();

    iterations
}

pub fn get_past_iterations(board_id: usize) -> Vec<Iteration> {
    get_iterations(vec![], board_id, State::Closed, 0, 50)
}

pub fn get_iteration_by_i(iterations: &[Iteration], i: usize) -> Iteration {
    iterations[i].clone()
}

pub fn get_iteration(sprint_id: usize) -> Result<Iteration, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let str_url = format!(
        "https://elsevier.atlassian.net/rest/agile/1.0/sprint/{}",
        sprint_id,
    );
    let url = reqwest::Url::parse(str_url.as_str()).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers(&config))
        .send()
        .expect("Failed to send request");

    response.json::<Iteration>()
}

pub fn get_current_iteration(board_id: usize) -> Vec<Iteration> {
    get_iterations(vec![], board_id, State::Active, 0, 50)
}

pub fn get_iterations_value(board_id: usize, state: State) -> Vec<Iteration> {
    get_iterations(vec![], board_id, state, 0, 50)
}

pub fn get_iterations(
    mut iterations: Vec<Iteration>,
    board_id: usize,
    state: State,
    start_at: usize,
    max_results: usize,
) -> Vec<Iteration> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let sa = start_at.to_string();
    let mx = max_results.to_string();
    let str_url = format!(
        "https://elsevier.atlassian.net/rest/agile/1.0/board/{}/sprint?state={}&startAt={}&maxResult={}",
        board_id, state, sa, mx
    );

    let url = reqwest::Url::parse(str_url.as_str()).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers(&config))
        .send()
        .expect("Failed to send request");

    match response.json::<Iterations>() {
        Ok(mut iters) => {
            for iter in iters.values.iter_mut() {
                if let Some(complete_date) = iter.complete_date {
                    iter.days = get_work_days(iter.start_date.unwrap(), complete_date);
                }
                iterations.push(iter.clone());
            }

            if !iters.is_last {
                let sa = start_at + max_results;
                iterations.append(&mut get_iterations(
                    iterations.clone(),
                    board_id,
                    state,
                    sa,
                    max_results,
                ));
            }
        }
        Err(e) => println!("{:?}", e),
    }

    iterations
}

fn get_work_days(start_date: DateTime<Utc>, end_date: DateTime<Utc>) -> i32 {
    let mut work_days = 0;

    let mut i = 0;
    loop {
        let new_date = start_date + Duration::days(i);

        match new_date.weekday() {
            Weekday::Sat => {}
            Weekday::Sun => {}
            _ => work_days += 1,
        }

        if new_date >= end_date {
            break;
        }
        i += 1;
    }

    work_days
}

pub fn get_sprint_issues(board_id: usize, sprint_id: usize) -> Result<Issues, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let str_url = format!(
        "https://elsevier.atlassian.net/rest/agile/1.0/board/{}/sprint/{}/issue",
        board_id, sprint_id
    );

    let query = vec![
        ("expand", "changelog"),
        (
            "fields",
            "resolution,status,issuetype,resolutiondate,summary,created,updated,flagged,customfield_10002",
        ),
    ];

    let url = reqwest::Url::parse(str_url.as_str()).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers(&config))
        .query(&query)
        .send()
        .expect("Failed to send request");

    response.json::<Issues>()
}

fn naive_date_to_naivedatime(naive: &chrono::NaiveDate) -> chrono::NaiveDateTime {
    naive.and_hms_opt(0, 0, 0).unwrap()
}

fn naive_datetime_to_datetime_utc(naive: &chrono::NaiveDateTime) -> chrono::DateTime<Utc> {
    let tz_offset = FixedOffset::east_opt(3600).unwrap();
    let td_with_tz: DateTime<FixedOffset> = tz_offset.from_local_datetime(naive).unwrap();

    Utc.from_utc_datetime(&td_with_tz.naive_utc())
}
