use crate::graphs::*;
use crate::issues::Issue;
use crate::plot::*;

pub fn create_pbileadtime_svg(team: &str, plot: &PBILeadTimePlot) -> String {
    const LINE_BUFFER: f64 = 45.0;
    let canvas_height = 600;
    let canvas_width = 1100;
    let chart_width = 900;
    let chart_height = 450.0;

    let mut g = Graph::new(canvas_height, canvas_width);

    let x_ratio = chart_width as f64 / plot.period.days as f64;
    g.middle.push_str("<g transform=\"translate(80,80)\">");

    //x axis begin
    g.middle.push_str(
        format!(
            "<g class=\"x axis\" transform=\"translate(0,{})\">",
            chart_height
        )
        .as_str(),
    );

    g.add_x_date_axis(
        chart_width as f64,
        plot.period.start.unwrap(),
        plot.period.end.unwrap(),
        5,
    );

    g.middle.push_str(
        format!(
            "<path class=\"domain\" d=\"M0,3V0H{}V3\" fill=\"#dadada\"></path>",
            chart_width
        )
        .as_str(),
    );

    g.middle.push_str("</g>"); //x axis end

    //y axis start
    let max_count = plot.value.iter().max_by_key(|x| x.age).unwrap();
    let ratio = chart_height / (max_count.age as f64 + 30.0);
    g.add_y_axis(chart_height, ratio, "Age (days)");
    //y axis end

    let mut p_y: f64 = chart_height;
    for l in &plot.lines {
        let y = chart_height - (l.age as f64 * ratio);
        let buf = y + LINE_BUFFER;

        //Remove lines which sits too close to each other
        if buf < p_y {
            p_y = buf;

            g.middle.push_str(
                format!(
                    "<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" stroke=\"#dadada\" stroke-dasharray=\"3\" />",
                    0, y, chart_width, y
                )
                .as_str(),
            );

            g.middle.push_str(
                format!(
                "<text class=\"line-text\" x=\"{}\" y=\"{}\" fill=\"#aaaaaa\">{} ({} days)</text>",
                chart_width + 8,
                y + 3.0,
                l.range,
                l.age
                )
                .as_str(),
            );

            g.middle.push_str(
                format!(
                "<text class=\"line-text\" x=\"{}\" y=\"{}\" fill=\"#aaaaaa\">{}/{} PBIs</text>",
                chart_width + 8,
                y + 14.0,
                l.count,
                l.total
                )
                .as_str(),
            );
        }
    }

    //plotting
    for p in &plot.value {
        let x = p.doy as f64 * x_ratio;
        let y = chart_height - (p.age as f64 * ratio);
        let issues = p.issues.join("&#10;");
        g.middle.push_str("<g>");
        g.middle.push_str(
            format!(
                "<circle cx=\"{}\" cy=\"{}\" r=\"7\" fill=\"#32a7e4\" issues=\"{}\">",
                x, y, issues,
            )
            .as_str(),
        );
        g.middle
            .push_str(format!("<title id=\"{}\">{}</title>", issues, issues).as_str());

        g.middle.push_str("</circle>");

        if p.count > 1 && p.count < 10 {
            g.middle.push_str(
                format!(
                    "<text class=\"plot-count\" x=\"{}\" y=\"{}\" fill=\"#fff\" issues=\"{}\">{}",
                    x - 2.0,
                    y + 3.0,
                    issues,
                    p.count,
                )
                .as_str(),
            );
            g.middle
                .push_str(format!("<title id=\"{}\">{}</title>", issues, issues).as_str());

            g.middle.push_str("</text>");
        }

        if p.count > 9 {
            g.middle.push_str(
                format!(
                    "<text class=\"plot-count\" x=\"{}\" y=\"{}\" fill=\"#fff\" issues=\"{}\">{}",
                    x - 5.0,
                    y + 3.0,
                    issues,
                    p.count,
                )
                .as_str(),
            );
            g.middle
                .push_str(format!("<title id=\"{}\">{}</title>", issues, issues).as_str());

            g.middle.push_str("</text>");
        }
        g.middle.push_str("</g>");
    }

    //Team name + type
    g.middle.push_str(
        format!(
            "<text y=\"500\" x=\"{}\" fill=\"#6f6f6f\" text-anchor=\"middle\" style=\"font-size: 1.5em\">",
            (chart_width as f64 / 2.0).round()
        )
        .as_str(),
    );
    g.middle.push_str(team);
    g.middle.push_str("</text>");

    g.middle.push_str("</g>");

    g.middle
        .push_str("<text y=\"20\" x=\"80\" fill=\"#6f6f6f\" text-anchor=\"right\">");
    g.middle.push_str(
        format!(
            "{} issues completed in this period: {} - {}",
            plot.count,
            plot.period.start.unwrap().format("%d-%m-%Y"),
            plot.period.end.unwrap().format("%d-%m-%Y")
        )
        .as_str(),
    );
    g.middle.push_str("</text>");

    g.middle
        .push_str("<text y=\"40\" x=\"80\" fill=\"#6f6f6f\" text-anchor=\"right\">");
    g.middle.push_str("</text>");

    let index_html = get_index();
    let index_html = index_html.replace("{title}", team);
    let index_html = index_html.replace("{svg}", &g.to_string());
    let mut content: String = String::new();
    content.push_str(&get_plotlines_table(&plot.lines));
    content.push_str(&get_issue_table(&plot.issues));

    index_html.replace("{content}", &content)
}

fn get_plotlines_table(lines: &Vec<PBILeadTimeLines>) -> String {
    let mut html: String = String::new();

    html.push_str("<table>");
    html.push_str("<tr>");
    html.push_str("<th>Range</th>");
    html.push_str("<th>Age</th>");
    html.push_str("<th>Count</th>");
    html.push_str("<th>Total</th>");
    html.push_str("</tr>");

    for l in lines {
        html.push_str("<tr>");
        html.push_str(format!("<td>{}</td>", l.range).as_str());
        html.push_str(format!("<td class='numbers'>{} days</td>", l.age).as_str());
        html.push_str(format!("<td class='numbers'>{}</td>", l.count).as_str());
        html.push_str(format!("<td class='numbers'>{}</td>", l.total).as_str());
        html.push_str("</tr>");
    }

    html.push_str("</table>");

    html
}

fn get_issue_table(issues: &Vec<Issue>) -> String {
    let mut html: String = String::new();

    html.push_str("<table>");

    html.push_str("<tr>");
    html.push_str("<th>Issue</th>");
    html.push_str("<th>Created</th>");
    html.push_str("<th>Activated</th>");
    html.push_str("<th>Closed</th>");
    html.push_str("<th>Cycle time</th>");
    html.push_str("<th>Lead time</th>");
    html.push_str("</tr>");

    for i in issues {
        html.push_str("<tr>");
        html.push_str(format!("<td>{}</td>", i.key).as_str());
        html.push_str("<td class='dates'>");
        if let Some(c) = i.fields.created {
            html.push_str(&c.format("%d-%m-%Y").to_string());
        }
        html.push_str("</td>");
        html.push_str("<td class='dates'>");
        if let Some(c) = i.fields.activated {
            html.push_str(&c.format("%d-%m-%Y").to_string());
        }
        html.push_str("</td>");

        html.push_str("<td class='dates'>");
        if let Some(c) = i.fields.closed {
            html.push_str(&c.format("%d-%m-%Y").to_string());
        }
        html.push_str("</td>");

        html.push_str("<td class='numbers'>");
        html.push_str(&get_time(i.fields.activated, i.fields.closed));
        html.push_str("</td>");

        html.push_str("<td class='numbers'>");
        html.push_str(&get_time(i.fields.created, i.fields.closed));
        html.push_str("</td>");

        html.push_str("</tr>");
    }

    html.push_str("</table>");

    html
}

fn get_time(start: Option<DateTime<Utc>>, stop: Option<DateTime<Utc>>) -> String {
    if let Some(ac) = start {
        if let Some(cl) = stop {
            let duration = cl - ac;
            let mut age = 1;

            if duration.num_days() > 0 {
                age = duration.num_days();
            }

            return format!("{}", age);
        }
    }

    String::from("-")
}
