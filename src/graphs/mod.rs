use crate::bucket::Bucket;
use chrono::prelude::*;
use chrono::Duration;
use std::fmt;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

pub mod monte_carlo;
pub mod pbileadtime;

#[derive(Clone, Debug, PartialEq)]
pub enum GType {
    None,
    LeadTime,
    SprintCycleTime,
    Throughput,
    PeriodThroughput,
    ThroughputStoryPoints,
    PeriodThroughputStoryPoints,
    CycleTime,
}

#[derive(Clone, Debug)]
pub struct Graph {
    start: String,
    head: String,
    pub middle: String,
    end: String,
    pub canvas_height: usize,
    pub canvas_width: usize,
}

#[derive(Clone, Debug)]
pub struct MCOptions {
    pub team: String,
    pub buckets: Vec<Bucket>,
    pub avg: f64,
    pub median: f64,
    pub sprint_len: usize,
    pub chance: f64,
    pub completed_day: f64,
    pub backlog_size: i32,
    pub working_days: i32,
    pub measurement: String,
}

#[derive(Clone, Debug, PartialEq)]
pub struct GraphType {
    pub key: String,
    pub name: String,
    pub gtype: GType,
}

impl Graph {
    pub fn new(canvas_height: usize, canvas_width: usize) -> Graph {
        Graph {
            start: get_start(canvas_height, canvas_width),
            head: get_head(),
            middle: String::new(),
            end: get_end(),
            canvas_height,
            canvas_width,
        }
    }

    pub fn add_x_date_axis(
        &mut self,
        chart_width: f64,
        start: NaiveDate,
        end: NaiveDate,
        modulo: i64,
    ) {
        let days = (end - start).num_days();
        let x_ratio = chart_width / days as f64;

        for i in 0..days {
            if i % modulo == 0 {
                let x_pos = i as f64 * x_ratio;
                let date = start + Duration::days(i);

                self.middle.push_str(
                    format!(
                        "<g class=\"tick\" transform=\"translate({},0)\" style=\"opacity: 1.0\">",
                        x_pos
                    )
                    .as_str(),
                );
                self.middle
                .push_str("<line x1=\"0\" y1=\"0\" y2=\"10\" x2=\"0\" stroke=\"#dadada\" stroke-width=\"3\"></line>");
                self.middle.push_str(
                    "<text dy=\".71em\" y=\"13\" x=\"0\" fill=\"#6f6f6f\" text-anchor=\"middle\">",
                );
                self.middle
                    .push_str(format!("{}", date.format("%d-%m")).as_str());
                self.middle.push_str("</text>");
                self.middle.push_str("</g>");
            }
        }
    }

    pub fn add_y_axis(&mut self, chart_height: f64, ratio: f64, unit: &str) {
        let mut y_start = 0.0;
        let y_max = 50.0;
        let y_tick_size = 50.0;
        let y_ticks = y_tick_size;
        let mut y_pos = chart_height;

        self.middle.push_str("<g class=\"y axis\">");
        loop {
            self.middle.push_str(
                format!(
                    "<g class=\"tick\" transform=\"translate(0,{})\" style=\"opacity: 1;\">",
                    y_pos
                )
                .as_str(),
            );
            self.middle.push_str("<line x2=\"-6\" y2=\"0\"></line>");
            self.middle.push_str(
                "<text dy=\".32em\" x=\"-9\" y=\"0\" fill=\"#6f6f6f\" text-anchor=\"end\">",
            );
            self.middle
                .push_str(format!("{}", (y_start / ratio) as usize).as_str());
            self.middle.push_str("</text>");
            self.middle.push_str("</g>");

            y_pos -= y_ticks;
            y_start += y_ticks;

            if y_pos <= y_max {
                break;
            }
        }

        self.middle.push_str(
            format!(
                "<path class=\"domain\" d=\"M-3,0H0V{}H-3\" fill=\"#dadada\"></path>",
                chart_height
            )
            .as_str(),
        );

        self.middle
            .push_str("<text y=\"50\" x=\"-27\" fill=\"#6f6f6f\">");
        self.middle.push_str(unit);
        self.middle.push_str("</text>");

        self.middle.push_str("</g>");
    }
}

impl fmt::Display for Graph {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut svg: String = String::new();
        svg.push_str(&self.start);
        svg.push_str(&self.head);
        svg.push_str(&self.middle);
        svg.push_str(&self.end);

        write!(f, "{}", svg)
    }
}

pub fn get_graph_types() -> Vec<GraphType> {
    vec![
        GraphType {
            key: "tp".into(),
            name: "Sprint throughput forecast".into(),
            gtype: GType::Throughput,
        },
        GraphType {
            key: "ptp".into(),
            name: "Period throughput forecast".into(),
            gtype: GType::PeriodThroughput,
        },
        GraphType {
            key: "tpsp".into(),
            name: "Sprint throughput story point forecast".into(),
            gtype: GType::ThroughputStoryPoints,
        },
        GraphType {
            key: "ptpsp".into(),
            name: "Period throughput story point forecast".into(),
            gtype: GType::PeriodThroughputStoryPoints,
        },
        GraphType {
            key: "lt".into(),
            name: "Lead time".into(),
            gtype: GType::LeadTime,
        },
        GraphType {
            key: "slt".into(),
            name: "Sprint Cycle time".into(),
            gtype: GType::SprintCycleTime,
        },
        GraphType {
            key: "ct".into(),
            name: "Cycle time".into(),
            gtype: GType::CycleTime,
        },
    ]
}

pub fn get_graph_type(types: Vec<GraphType>, i: usize) -> GraphType {
    types[i].clone()
}

pub fn get_graph_type_by_key(types: Vec<GraphType>, s: &str) -> Option<GraphType> {
    types.iter().find(|&x| x.key == s).cloned()
}

impl fmt::Display for GraphType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

fn get_start(canvas_height: usize, canvas_width: usize) -> String {
    let mut svg: String = String::new();
    svg.push_str("<?xml version=\"1.0\" standalone=\"yes\"?>");
    svg.push_str("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">");
    svg.push_str(
        format!(
            "<svg width=\"{}\" height=\"{}\" xmlns=\"http://www.w3.org/2000/svg\">",
            canvas_width, canvas_height
        )
        .as_str(),
    );

    svg
}

fn get_head() -> String {
    let mut svg: String = String::new();
    svg.push_str("<style>");
    svg.push_str("text { font-family: arial; font-size: 1em; }");
    svg.push_str("text.plot-count { font-family: arial; font-size: 0.55em; }");
    svg.push_str("text.line-text { font-family: arial; font-size: 0.55em; }");
    svg.push_str("</style>");

    svg
}

fn get_end() -> String {
    String::from("</svg>")
}

fn get_root_folder() -> PathBuf {
    let exe = std::env::current_exe().unwrap();
    exe.parent().unwrap().to_path_buf()
}

fn get_templates() -> PathBuf {
    get_root_folder().join("templates")
}

fn get_file_content(path: PathBuf) -> String {
    let mut partial_file = File::open(path).unwrap();
    let mut partial = String::new();
    partial_file.read_to_string(&mut partial).unwrap();

    partial
}

fn get_index() -> String {
    let path = get_templates().join("index.html");

    get_file_content(path)
}
