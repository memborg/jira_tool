use crate::issues::Issue;
use crate::period::Period;

#[derive(Debug, Clone, Serialize)]
pub struct PBIAge {
    pub dow: usize,
    pub age: usize,
    pub count: usize,
}

#[derive(Debug, Clone, Serialize)]
pub struct PBIAgePlot {
    pub value: Vec<PBIAge>,
    pub count: usize,
}

#[derive(Debug, Clone, Serialize)]
pub struct PBILeadTime {
    pub doy: usize,
    pub age: usize,
    pub count: usize,
    pub issues: Vec<String>,
}

#[derive(Debug, Clone, Serialize)]
pub struct PBILeadTimePlot {
    pub value: Vec<PBILeadTime>,
    pub lines: Vec<PBILeadTimeLines>,
    pub count: usize,
    pub total: usize,
    pub period: Period,
    pub issues: Vec<Issue>,
}

#[derive(Debug, Clone, Serialize)]
pub struct PBILeadTimeLines {
    pub count: usize,
    pub total: usize,
    pub age: usize,
    pub range: String,
    pub num: usize,
}
